﻿using CliWrap;
using dd2vttopt.Models;
using System;
using System.IO;
using System.Threading.Tasks;

namespace dd2vttopt
{
    internal static class Program
    {
        private static async Task Main(string[] args)
        {
            string fileName;

            if (args.Length > 0)
            {
                fileName = args[0];

                Console.WriteLine("Converting: " + fileName);

                Dd2Vtt inputFile = Dd2Vtt.FromJson(File.ReadAllText(fileName));

                string filePath = fileName.Split(".dd2vtt")[0] + ".png";

                File.WriteAllBytes(filePath, Convert.FromBase64String(inputFile.Image));

                string cmd = "convert " + filePath + " - quality 50 - define webp: lossless = true " + filePath.Split(".png")[0] + ".webp";

                try
                {
                    var result = await Cli.Wrap("magick")
                        .WithArguments(cmd)
                        .WithValidation(CommandResultValidation.None)
                        .ExecuteAsync();

                    if (result.ExitCode == 1)
                    {
                        inputFile.Image = Convert.ToBase64String(File.ReadAllBytes(filePath.Split(".png")[0] + ".webp"));

                        File.WriteAllText(fileName, inputFile.ToJson());

                        File.Delete(filePath);
                        File.Delete(filePath.Split(".png")[0] + ".webp");

                        Console.WriteLine("Finished: " + fileName);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            else
            {
                Console.WriteLine("Provide a file name");
            }
        }
    }
}
