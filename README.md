Compresses the image embedded in dds2vtt files to 50% lossy webp.
Drag the dd2vtt file onto the executable to perform the conversion.

Requires https://imagemagick.org/script/download.php

Does almost no error checking, written very quickly.